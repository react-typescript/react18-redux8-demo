import classNames from 'classnames';
import React from 'react';
import { useSelector } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { RootState } from '../../App';

const activeStyle: React.CSSProperties = { color: 'orange'};

export const Navbar: React.FC = () => {
  const theme = useSelector((state: RootState) => state.app.theme)
  const language = useSelector((state: RootState) => state.app.language)

  return (
    <nav
      className={classNames(
        'navbar navbar-expand',
        { 'navbar-light bg-light': theme === 'light'},
        { 'navbar-dark bg-dark': theme === 'dark'},
      )}
    >
      <div className="navbar-brand">
        <NavLink
          style={({ isActive }) =>
            isActive ? activeStyle : {}
          }
          className="nav-link"
          to="/">
          REDUX
          {language === 'it' ? '🇮🇹' : '🇬🇧'}
        </NavLink>
      </div>

      <div className="collapse navbar-collapse" id="navbarNav">
        <ul className="navbar-nav">
          <li className="nav-item">
            <NavLink
              style={({ isActive }) =>
                isActive ? activeStyle : {}
              }
              className="nav-link"
              to="/settings">
              <small>settings</small>
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink
              style={({ isActive }) =>
                isActive ? activeStyle : {}
              }
              className="nav-link"
              to="/counter">
              <small>counter</small>
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink
              style={({ isActive }) =>
                isActive ? activeStyle : {}
              }
              className="nav-link"
              to="/users">
              <small>users</small>
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink
              style={({ isActive }) =>
                isActive ? activeStyle : {}
              }
              className="nav-link"
              to="/catalog">
              <small>catalog</small>
            </NavLink>
          </li>

        </ul>
      </div>
    </nav>
  )
}
