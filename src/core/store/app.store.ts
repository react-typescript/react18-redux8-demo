// core/store/config.store.ts
import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { Config, Language, Theme } from '../../model/config';

const initialState: Config = { language: 'it', theme: 'dark' };

export const appStore = createSlice({
  name: 'home',
  initialState,
  reducers: {
    changeTheme(state, action: PayloadAction<Theme>) {
      state.theme = action.payload;
    },
    changeLanguage(state, action: PayloadAction<Language>) {
      state.language = action.payload;
    },

  }
});

export const { changeLanguage, changeTheme } = appStore.actions
