// home/HomePage.tsx
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { addNews, deleteNews, toggleNews } from './store/news.store';
import { setFilter } from './store/news-filter.store';
import { getNewsList } from './store/news.selectors';

export const HomePage = () => {
  const newsList = useSelector(getNewsList)
  const dispatch = useDispatch();

  function onKeyDownHandler(e: React.KeyboardEvent<HTMLInputElement>) {
    if (e.key === 'Enter') {
      dispatch(addNews(e.currentTarget.value))
      e.currentTarget.value = '';
    }
  }

  function setVisibilityFilter(e: React.ChangeEvent<HTMLSelectElement>) {
    dispatch(setFilter({ published: e.currentTarget.value }));
  }

  return <div>
    <h1>CRUD CLIENT SIDE (no server)</h1>
    <input type="text" className="form-control" onKeyDown={onKeyDownHandler} placeholder="News Title (press enter)"/>

    {/*Show select when newsList contains product*/}
    <select className="form-control" onChange={setVisibilityFilter}>
      <option value="all">Show all</option>
      <option value="published">Published only</option>
      <option value="unpublished">Unpublished only</option>
    </select>
    {
      newsList.map(news => {
        return (
          <li className="list-group-item d-flex justify-content-between" key={news.id}>
            <div>{news.title}</div>

            <div
              className="badge bg-dark ms-3"
              onClick={() => dispatch(toggleNews(news.id))}
            >
              { news.published ? 'PUBLISHED' : 'UNPUBLISHED'}
            </div>

            <div className="pull-right">
              <i className="fa fa-trash" onClick={() => dispatch(deleteNews(news.id))}></i>
            </div>
          </li>
        )
      })
    }
  </div>
};
