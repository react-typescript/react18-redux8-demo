import { RootState } from '../../../App';

export const getNewsList = (state: RootState) => {
  switch(state.home.filters.published) {
    case 'published':    return state.home.list.filter(n => n.published);
    case 'unpublished':    return state.home.list.filter(n => !n.published);
    default: return state.home.list
  }
}
