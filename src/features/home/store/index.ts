// home/store/index.ts
import { combineReducers } from '@reduxjs/toolkit';
import { newsStore } from './news.store';
import { newsFilterStore } from './news-filter.store';

export const homeReducers = combineReducers({
  list: newsStore.reducer,
  filters: newsFilterStore.reducer
})
