// home/store/news-filter.store.ts
import { createSlice, PayloadAction } from '@reduxjs/toolkit'

export interface NewsFilterState {
  published: string; // 'all' | 'published' | 'unpublished',
}

export const newsFilterStore = createSlice({
  name: 'home',
  initialState: { published: 'all' },
  reducers: {
    setFilter(state, filter: PayloadAction<Partial<NewsFilterState>>) {
      return {
        ...state,
        ...filter.payload
      }
    }
  }
});

export const { setFilter} = newsFilterStore.actions
