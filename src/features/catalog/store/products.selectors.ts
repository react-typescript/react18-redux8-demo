import { RootState } from '../../../App';
import { Product } from '../model/product';

// UPDATED
export const selectProductsList = (state: RootState) => state.catalog.list.entities;
// UPDATED
export const selectProductsTotal = (state: RootState) => state.catalog.list.entities.reduce((acc: number, cur: Product) => {
  return acc + cur.price;
}, 0);

// UPDATED
export const selectProductsByCat = (state: RootState) => {
  const activeCategoryId = state.catalog.categories.activeCategoryId;
  // return selectProductsList(state)    ===> alternative version using another selector
  return state.catalog.list.entities
    .filter(p => activeCategoryId === -1 ? true : p.categoryID === activeCategoryId);
}

export const selectCategories = ((state: RootState) => state.catalog.categories.list)
export const selectActiveCategoryId = ((state: RootState) => state.catalog.categories.activeCategoryId)

export const selectProductsError = (state: RootState) => state.catalog.list.error;
