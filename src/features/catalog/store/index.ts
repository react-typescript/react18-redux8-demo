// features/catalog/store/index.ts
import { combineReducers } from '@reduxjs/toolkit';
import { productsStore } from './products.store';
import { categoriesStore } from './categories.store';

export const catalogReducers = combineReducers({
  list: productsStore.reducer,
  categories: categoriesStore.reducer
});
