import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { Product } from '../model/product';
import { addProduct, getProducts } from './products.actions';

export const productsStore = createSlice({
  name: 'products',
  initialState: {
    entities: [] as Product[],
    error: false,
  },
  reducers: {
    getProductsSuccess(state, action: PayloadAction<Product[]>) {
      // without ImmerJS
      /*return {
        ...state,
        list: action.payload
      }*/
      // with ImmerJS
      state.error = false;
      state.entities = action.payload
    },
    addProductSuccess(state, action: PayloadAction<Product>) {
      state.error = false;
      state.entities.push(action.payload)
    },
    deleteProductSuccess(state, action: PayloadAction<number>) {
      state.error = false;
      const index = state.entities.findIndex(p => p.id === action.payload)
      state.entities.splice(index, 1)
    },
    toggleProductVisibility(state, action: PayloadAction<Product>) {
      state.error = false;
      const product = state.entities.find(p => p.id === action.payload.id);
      if (product) {
        product.visibility = action.payload.visibility;
      }
    },
    /*setError(state, action: PayloadAction<boolean>) {
      state.error = true;
    }*/
  },
  extraReducers: builder =>
    builder
      .addCase(getProducts.rejected, (state, action) => {
        state.error = true;
      })
      .addCase(addProduct.rejected, (state, action) => {
        state.error = true;
      })
      .addCase(getProducts.fulfilled, (state, action) => {
        state.error = false;
      })
      .addCase(addProduct.fulfilled, (state, action) => {
        state.error = false;
      })
});

export const {
  addProductSuccess,
  getProductsSuccess,
  toggleProductVisibility,
  deleteProductSuccess,
} = productsStore.actions;
