// features/catalog/store/categories.store.ts

import { createSlice, PayloadAction } from '@reduxjs/toolkit';

export const categoriesStore = createSlice({
  name: '[products categories]',
  initialState: {
    list: [
      { id: -1, name: 'All'},
      { id: 1, name: 'Latticini'},
      { id: 2, name: 'Frutta'},
    ],
    activeCategoryId: -1
  },
  reducers: {
    // NEW
    changeCategoryId(state, action: PayloadAction<number>) {
      return ({ ...state, activeCategoryId: action.payload })
    }
  }
})

export const { changeCategoryId } = categoriesStore.actions;
