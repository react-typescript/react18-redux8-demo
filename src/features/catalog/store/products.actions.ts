import { AppThunk } from '../../../App';
import Axios from 'axios';
import { Product } from '../model/product';
import {
  addProductSuccess,
  deleteProductSuccess,
  getProductsSuccess,
  toggleProductVisibility
} from './products.store';
import { createAsyncThunk } from '@reduxjs/toolkit';

// catalog/store/products.actions.ts
// ...
export const getProducts = createAsyncThunk<Product[], void>(
  'products/get',
  async (payload, {  dispatch, rejectWithValue })  => {
    try {
      const response = await Axios.get<Product[]>('http://localhost:3001/products')
      dispatch(getProductsSuccess(response.data))
      return response.data;
    } catch (err) {
      return rejectWithValue('errore!')
    }
  }
)
// ...

export const deleteProduct = (id: number): AppThunk => async dispatch => {
  try {
    await Axios.delete(`http://localhost:3001/products/${id}`);
    dispatch(deleteProductSuccess(id))
  } catch (err) {
  }
};

export const addProduct = createAsyncThunk<Product, Partial<Product>>(
  'products/add',
  async (payload, {  dispatch, rejectWithValue })  => {
    try {
      const newProduct = await Axios.post<Product>(
        'http://localhost:3001/products',
        { ...payload, visibility: false }
      );
      dispatch(addProductSuccess(newProduct.data))
      return newProduct.data;
    } catch (err) {
      return rejectWithValue('errore!')
    }
  }
)


export const toggleProduct = (product: Product): AppThunk => async dispatch => {
  try {
    const updatedProduct: Product = {
      ...product,
      visibility: !product.visibility
    };
    const response = await Axios.patch<Product>(`http://localhost:3001/products/${product.id}`, updatedProduct);
    dispatch(toggleProductVisibility(response.data))
  } catch (err) {
  }
};
