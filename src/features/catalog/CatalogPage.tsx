import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, useAppDispatch } from '../../App';
import * as ProductActions from './store/products.actions';
import {
  selectActiveCategoryId,
  selectCategories,
  selectProductsByCat, selectProductsError,
  selectProductsTotal
} from './store/products.selectors';
import { ProductsList, ProductsForms } from './components';
import { CategoryFilters } from './components/CategoryFilters';
import { changeCategoryId } from './store/categories.store';
import { unwrapResult } from '@reduxjs/toolkit';

export const CatalogPage = () => {
  const categories = useSelector(selectCategories);  const total = useSelector(selectProductsTotal);
  const dispatch = useAppDispatch();
  // const dispatchAlternative = useDispatch<AppDispatch>()
  const activeCategoryID = useSelector(selectActiveCategoryId)
  const products = useSelector(selectProductsByCat);
  // NEW
  const errors = useSelector(selectProductsError);


  useEffect(() => {
    dispatch(ProductActions.getProducts())
      .then(unwrapResult)
      .then(res => console.log('action response', res))
      .catch(err => console.log('ERROR:', err))
  }, [dispatch])

  return <div>
    { errors &&  <div className="alert alert-danger">server side error</div>}

    <ProductsForms
      categories={categories}
      onSubmit={(p) => dispatch(ProductActions.addProduct(p))} />

    <hr/>

    <CategoryFilters
      categories={categories}
      selectCategory={(id) => dispatch(changeCategoryId(id))}
      activeCategoryID={activeCategoryID} />

    <hr/>
    <ProductsList
      total={total}
      products={products}
      toggle={todo=>  dispatch(ProductActions.toggleProduct(todo))}
      delete={id => dispatch(ProductActions.deleteProduct(id))}
    />
  </div>
};
