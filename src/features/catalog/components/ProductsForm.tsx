import React, { useState } from 'react';
import classnames from 'classnames';
import { Product } from '../model/product';

interface ProductsFormProps {
  // NEW
  categories: Array<{ id: number, name: string}>;
  onSubmit: (product: Partial<Product>) => void;
}

export const ProductsForms = (props: ProductsFormProps) => {
  // NEW
  const [data, setData] = useState<Partial<Product>>({ title: '', price: 0, categoryID: -1});
  const titleIsValid = data.title && data.title.length > 3;
  const priceIsValid = data.price && data.price > 0;
  // NEW
  const isCategoryValid = data.categoryID !== -1;
  const valid = titleIsValid && priceIsValid && isCategoryValid;

  // 5. UPDATED
  const onChangeHandler = (e: React.FormEvent<HTMLInputElement | HTMLSelectElement>) => {
    // 6. UPDATED: otherwise price is saved as string
    const value = e.currentTarget.value;
    const type = e.currentTarget.type;
    setData({
      ...data,
      [e.currentTarget.name]:
        type === 'number' || type === 'select-one' ? +value : value
    })
  };

  const onSubmitHandler = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    // UPDATED
    props.onSubmit(data);
    setData({ title: '', price: 0});
  };

  return (
    <div className="example">
      <form onSubmit={onSubmitHandler}>
        {/*UPDATED*/}
        <input
          className={classnames(
            'form-control',
            { 'is-valid': titleIsValid },
            { 'is-invalid': !titleIsValid },
          )}
          type="text"
          placeholder="Write something..."
          onChange={onChangeHandler}
          name="title"
          value={data.title}
        />

        {/*NEW*/}
        <input
          className={classnames(
            'form-control',
            { 'is-valid': priceIsValid },
            { 'is-invalid': !priceIsValid },
          )}
          type="number"
          placeholder="Write something..."
          onChange={onChangeHandler}
          name="price"
          value={data.price}
        />
        <select
          className={classnames(
            'form-control',
            { 'is-valid': isCategoryValid },
            { 'is-invalid': !isCategoryValid },
          )}
          name="categoryID"
          value={data.categoryID}
          onChange={onChangeHandler}
        >
          <option value={-1}>Select a category</option>
          {
            props.categories.map(c => {
              return <option key={c.id} value={c.id}>{c.name}</option>
            })
          }
        </select>

        <button
          className="btn btn-primary"
          type="submit"
          disabled={!valid}>ADD</button>
      </form>
    </div>
  );
};
