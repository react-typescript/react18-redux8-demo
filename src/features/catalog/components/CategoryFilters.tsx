// catalog/components/CategoryFilters.tsx

import React from 'react';
import classNames from 'classnames';

interface CategoryFiltersProps {
  categories: Array<{ id: number, name: string}>;
  activeCategoryID: number;
  selectCategory: (id: number) => void;
}

export const CategoryFilters: React.FC<CategoryFiltersProps> = props => {
  return <>
    {
      props.categories.map(c => {
        return (
          <button
            key={c.id}
            className={classNames(
              'btn',
              { 'btn-primary': c.id === props.activeCategoryID },
              { 'btn-outline-primary': c.id !== props.activeCategoryID }
            )}
            onClick={() => props.selectCategory(c.id)}
          >
            {c.name}
          </button>
        )
      })
    }
  </>
}
