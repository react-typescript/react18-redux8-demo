import React  from 'react';
import { Product } from '../model/product';

interface ProductsListProps {
  total: number;
  products: Product[];
  toggle: (product: Product) => void
  delete: (productId: number) => void
}

export const ProductsList: React.FC<ProductsListProps> = (props) => {
  const deleteHandler = (event: React.MouseEvent<HTMLElement>, id: number) => {
    event.stopPropagation();
    props.delete(id);
  };

  return (
    <>
      {
        props.products.map((product) => {
          return (
            <li
              className="list-group-item d-flex justify-content-between"
              style={{ opacity: product.visibility ? 1 : 0.5}}
              key={product.id}
            >
              <div className="ml-2">
              <span onClick={() => props.toggle(product)}>
              {
                product.visibility ?
                  <i className="fa fa-eye" /> :
                  <i className="fa fa-eye-slash" />
              }
                { product.title } Cat: {product.categoryID}
              </span>
              </div>
              <div>
                <span>€ {product.price}</span>
                <i
                  className="fa fa-trash ms-2"
                  onClick={(e) => deleteHandler(e, product.id)}
                />
              </div>
            </li>
          )
        })
      }

      <div className="text-center">
        <div className="badge bg-info">Total: € {props.total}</div>
      </div>
    </>
  )
};
