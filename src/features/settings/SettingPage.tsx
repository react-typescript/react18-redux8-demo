import classNames from 'classnames';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../App';
import { changeLanguage, changeTheme } from '../../core/store/app.store';

export const SettingsPage = () => {
  const dispatch = useDispatch();
  const theme = useSelector((state: RootState) => state.app.theme)
  const language = useSelector((state: RootState) => state.app.language)

  return <div className="mt-4">
    <h1>Settings</h1>

    <div className="btn-group">
      <button
        className={classNames('btn btn-light', { 'bg-warning': theme === 'dark'})}
        onClick={() => dispatch(changeTheme('dark'))}
      >Set Dark</button>
      <button
        className={classNames('btn btn-light', { 'bg-warning': theme === 'light'})}
        onClick={() => dispatch(changeTheme('light'))}
      >Set Light</button>
    </div>

    <br/>

    <div className="btn-group">
      <button
        className={classNames('btn btn-light', { 'bg-warning': language === 'it'})}
        onClick={() => dispatch(changeLanguage('it'))
      }>Italian</button>
      <button
        className={classNames('btn btn-light', { 'bg-warning': language === 'en'})}
        onClick={() => dispatch(changeLanguage('en'))
      }>English</button>
    </div>
  </div>
};
