// features/counter/store/counter.selectors.ts
import { RootState } from '../../../../App';

export const getCounter = (state: RootState) => state.counter.value;
export const getItemsPerPallet = (state: RootState) => state.counter.config.itemsPerPallet;
export const getTotalPallets = (state: RootState) => {
  return Math.ceil(state.counter.value / state.counter.config.itemsPerPallet)
};
export const getMaterial = (state: RootState) => state.counter.config.material;
