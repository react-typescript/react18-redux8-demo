import { combineReducers } from '@reduxjs/toolkit';
import { counterReducer } from './counter/counter.reducer';
import { configReducer } from './config/config.reducer';

export const counterReducers = combineReducers({
  value: counterReducer,
  config: configReducer
})
