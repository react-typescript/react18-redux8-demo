import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { decrement, increment, reset } from './store/counter/counter.actions';
import { RootState } from '../../App';
import { setConfig } from './store/config/config.actions';
import { getCounter, getItemsPerPallet, getMaterial, getTotalPallets } from './store/counter/counter.selectors';


export const CounterPage = () => {
  const dispatch = useDispatch();
  const counter = useSelector(getCounter)
  const itemsPerPallet = useSelector(getItemsPerPallet);
  const material = useSelector(getMaterial);
  const totalPallets = useSelector(getTotalPallets)

  return <div>
    <h1>Counter {counter}</h1>
    <h2>Pallet: {totalPallets} ({itemsPerPallet} items per pallet) - {material} </h2>
    <button onClick={() => dispatch(decrement(5))}>-</button>
    <button onClick={() => dispatch(increment(10))}>+</button>
    <button onClick={() => dispatch(reset())}>reset</button>
    <hr/>
    <button onClick={() => dispatch(setConfig({ itemsPerPallet: 5}))}>Set 5 items per pallet</button>
    <button onClick={() => dispatch(setConfig({ itemsPerPallet: 12}))}>Set 12 items per pallet</button>
    <button onClick={() => dispatch(setConfig({ material: 'plastic'}))}>Set material plastic</button>
    <button onClick={() => dispatch(setConfig({ material: 'wood'}))}>Set material wood</button>
  </div>
};
