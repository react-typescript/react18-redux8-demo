export interface Config {
  itemsPerPallet: number;
  material: string; // wood | plastic
}
