import React from 'react';
import { BrowserRouter, Navigate, Route, Routes } from 'react-router-dom';
import { Provider, useDispatch } from 'react-redux';
import { Action, AnyAction, combineReducers, configureStore, ThunkAction, ThunkDispatch } from '@reduxjs/toolkit';
import './App.css';
import { appStore } from './core/store/app.store';
import { CatalogPage } from './features/catalog/CatalogPage';
import { CounterPage } from './features/counter/CounterPage';
import { Navbar } from './core/components/Navbar';
import { configReducer } from './features/counter/store/config/config.reducer';
import { HomePage } from './features/home/HomePage';
import { SettingsPage } from './features/settings/SettingPage';
import { UsersPage } from './features/users/UsersPage';

import { counterReducers } from './features/counter/store';
import { homeReducers } from './features/home/store';
import { catalogReducers } from './features/catalog/store';

// Define the store structure
const rootReducer = combineReducers({
  app: appStore.reducer,
  home: homeReducers,
  counter: counterReducers,
  catalog: catalogReducers
});

// Create the type of store based on rootReducer
export type RootState = ReturnType<typeof rootReducer>

export type AppThunk = ThunkAction<void, RootState, null, Action<string>>;
export type AppDispatch = ThunkDispatch<RootState, any, AnyAction>;

// create hook
export const useAppDispatch = () => useDispatch<AppDispatch>()

// Configure Store
export const store = configureStore({
  reducer: rootReducer,
  devTools: process.env.NODE_ENV !== 'production',
});

function App() {
  return (
    <Provider store={store}>
      <BrowserRouter>

        <Navbar />

        <Routes>
          <Route path="/home" element={<HomePage />} />
          <Route path="/counter" element={<CounterPage />} />
          <Route path="/users" element={<UsersPage />} />
          <Route path="/catalog" element={<CatalogPage />} />
          <Route path="/settings" element={<SettingsPage />} />
          <Route
            path="*"
            element={
              <Navigate to='/home' />
            }
          />
        </Routes>
      </BrowserRouter>

    </Provider>
  );
}

export default App;
